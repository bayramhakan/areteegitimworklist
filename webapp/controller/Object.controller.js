/*global location*/
sap.ui.define([
	"zepm/so/wl/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"zepm/so/wl/model/formatter",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(
	BaseController,
	JSONModel,
	History,
	formatter,
	MessageBox,
	Filter,
	FilterOperator
) {
	"use strict";

	return BaseController.extend("zepm.so.wl.controller.Object", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function() {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var iOriginalBusyDelay,
				oViewModel = new JSONModel({
					busy: true,
					delay: 0
				});
			//Bayram Hakan
			this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.getView()));
			this.oModel = this.oComponent.getModel();
			//Bayram Hakan
			
			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

			// Store original busy indicator delay, so it can be restored later on
			iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
			this.setModel(oViewModel, "objectView");
			this.getOwnerComponent().getModel().metadataLoaded().then(function() {
				// Restore original busy indicator delay for the object view
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			});
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Event handler when the share in JAM button has been clicked
		 * @public
		 */
		onShareInJamPress: function() {
			var oViewModel = this.getModel("objectView"),
				oShareDialog = sap.ui.getCore().createComponent({
					name: "sap.collaboration.components.fiori.sharing.dialog",
					settings: {
						object: {
							id: location.href,
							share: oViewModel.getProperty("/shareOnJamTitle")
						}
					}
				});
			oShareDialog.open();
		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			var sObjectId = oEvent.getParameter("arguments").objectId;

			this.getModel().metadataLoaded().then(function() {
				var sObjectPath = this.getModel().createKey("SoheaderdataSet", {
					SoId: sObjectId
				});
				//Bayram Hakan
				this.oTable = this.byId(sap.ui.core.Fragment.createId("idSOItemsFramgent", "idSOItemsFragmentList"));
				this.sObjectPath = sObjectPath;
				this._getHeaderItems(sObjectPath);
				//Bayram Hakan
				this._bindView("/" + sObjectPath);
			}.bind(this));
		},

		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound
		 * @private
		 */
		_bindView: function(sObjectPath) {
			var oViewModel = this.getModel("objectView"),
				oDataModel = this.getModel();

			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function() {
						oDataModel.metadataLoaded().then(function() {
							// Busy indicator on view should only be set if metadata is loaded,
							// otherwise there may be two busy indications next to each other on the
							// screen. This happens because route matched handler already calls '_bindView'
							// while metadata is loaded.
							oViewModel.setProperty("/busy", true);
						});
					},
					dataReceived: function() {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},

		_onBindingChange: function() {
			var oView = this.getView(),
				oViewModel = this.getModel("objectView"),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("objectNotFound");
				return;
			}

			var oResourceBundle = this.getResourceBundle(),
				oObject = oView.getBindingContext().getObject(),
				sObjectId = oObject.SoId,
				sObjectName = oObject.BuyerName;

			// Everything went fine.
			oViewModel.setProperty("/busy", false);
			oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("saveAsTileTitle", [sObjectName]));
			oViewModel.setProperty("/shareOnJamTitle", sObjectName);
			oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage",
				oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
		},
		_getHeaderItems: function(objectPath) {

			//Controller üzerinden Odata servisine gidilip kalemler alınır , 
			//JSON Modele set edilir.

			var t = this;

			if (!this.SOItemsFragment) {
				this.SOItemsFragment = sap.ui.xmlfragment("zepm.so.wl.view.fragments.SOItems", t);
			}

			this.SOItemsFragment.setBusy(true);

			var mParameters = {
				success: function(d) {

					sap.ui.core.BusyIndicator.hide();
					
					//Delivery Date i burda değiştirdik çünkü filtreleme yapmak için değişen dataya ihtiyacımız vardır , date Input kullanmamak için.
					for (var i=0 ; i<d.results.length ; i++){
						d.results[i].DeliveryDate = formatter.dateFormat(d.results[i].DeliveryDate);
					}
					
					var jsonModel = new sap.ui.model.json.JSONModel({
						SOItemCollection: d.results
					});
					t.oTable.setModel(jsonModel, "SOItemsModel");
					t.SOItemsFragment.setBusy(false);
				},
				error: function(e) {
					sap.ui.core.BusyIndicator.hide();
					t.SOItemsFragment.setBusy(false);
					MessageBox.alert(e.response.body);
				}
			};
			sap.ui.core.BusyIndicator.show(0);
			this.oModel.read("/" + objectPath + "/SOHeaderTOItems", mParameters);

		},
		onRefreshData: function(oEvent) {
			//Odatadan en güncel olan listeyi alır. 
			this._getHeaderItems(this.sObjectPath);
		},
		onSearch: function(oEvent) {

			//Worklist deki ile aynı mantıkta çalışır . Ama Client Side filtre uygular , çünkü JSON Model e set ettik.

			if (oEvent.getParameters().refreshButtonPressed) {
				this.onRefreshData();
			} else {
				var oTableSearchState = [];
				var sQuery = oEvent.getParameter("query") || oEvent.getParameter("newValue"); // query isEmpty ise bir yandaki değeri alacak.

				if (sQuery && sQuery.length > 0) {
			
			// 3 Tane filtre OR ile oldu.
			
					var f = new sap.ui.model.Filter({
						filters: [new sap.ui.model.Filter("ProductId", FilterOperator.Contains, sQuery),
							new sap.ui.model.Filter("DeliveryDate", FilterOperator.Contains, sQuery) ,
							new sap.ui.model.Filter("CurrencyCode", FilterOperator.Contains, sQuery)
						],
						bAnd: false
					});

					oTableSearchState = [f];
				}
				this.oTable.getBinding("rows").filter(oTableSearchState);
			}
		},
		onNavBack: function() {
			window.history.go(-1);
		},
		
		//Bayram Hakan
	});

});