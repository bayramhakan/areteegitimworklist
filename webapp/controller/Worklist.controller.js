sap.ui.define([
	"zepm/so/wl/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"zepm/so/wl/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(BaseController, JSONModel, History, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("zepm.so.wl.controller.Worklist", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function() {
			var oViewModel,
				iOriginalBusyDelay,
				oTable = this.byId("table");

			// Put down worklist table's original value for busy indicator delay,
			// so it can be restored later on. Busy handling on the table is
			// taken care of by the table itself.
			iOriginalBusyDelay = oTable.getBusyIndicatorDelay();
			this._oTable = oTable;
			// keeps the search state
			this._oTableSearchState = [];

			//Bayram Hakan
			this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.getView()));
			this.oModel = this.oComponent.getModel();
			//Bayram Hakan

			// Model used to manipulate control states
			oViewModel = new JSONModel({
				worklistTableTitle: this.getResourceBundle().getText("worklistTableTitle"),
				saveAsTileTitle: this.getResourceBundle().getText("saveAsTileTitle", this.getResourceBundle().getText("worklistViewTitle")),
				shareOnJamTitle: this.getResourceBundle().getText("worklistTitle"),
				shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
				shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
				tableNoDataText: this.getResourceBundle().getText("tableNoDataText"),
				tableBusyDelay: 0
			});
			this.setModel(oViewModel, "worklistView");

			// Make sure, busy indication is showing immediately so there is no
			// break after the busy indication for loading the view's meta data is
			// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
			oTable.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for worklist's table
				oViewModel.setProperty("/tableBusyDelay", iOriginalBusyDelay);
			});
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Triggered by the table's 'updateFinished' event: after new table
		 * data is available, this handler method updates the table counter.
		 * This should only happen if the update was successful, which is
		 * why this handler is attached to 'updateFinished' and not to the
		 * table's list binding's 'dataReceived' method.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {
			// update the worklist's object counter after the table update
			var sTitle,
				oTable = oEvent.getSource(),
				iTotalItems = oEvent.getParameter("total");
			// only update the counter if the length is final and
			// the table is not empty
			if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
			} else {
				sTitle = this.getResourceBundle().getText("worklistTableTitle");
			}
			this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
		},

		/**
		 * Event handler when a table item gets pressed
		 * @param {sap.ui.base.Event} oEvent the table selectionChange event
		 * @public
		 */
		onPress: function(oEvent) {
			// The source is the list item that got pressed
			this._showObject(oEvent.getSource());
		},

		/**
		 * Event handler when the share in JAM button has been clicked
		 * @public
		 */
		onShareInJamPress: function() {
			var oViewModel = this.getModel("worklistView"),
				oShareDialog = sap.ui.getCore().createComponent({
					name: "sap.collaboration.components.fiori.sharing.dialog",
					settings: {
						object: {
							id: location.href,
							share: oViewModel.getProperty("/shareOnJamTitle")
						}
					}
				});
			oShareDialog.open();
		},

		onSearch: function(oEvent) {
			if (oEvent.getParameters().refreshButtonPressed) {
				this.onRefresh();
			} else {
				var oTableSearchState = [];
				var sQuery = oEvent.getParameter("query");

				if (sQuery && sQuery.length > 0) {
					//Bayram Hakan
					var sProductId = this.byId("idProductIDInput").getValue();
					oTableSearchState = [new Filter("BuyerName", FilterOperator.Contains, sQuery) , new Filter("ProductId", FilterOperator.Contains, sProductId)];
					//Bayram Hakan
				}
				this._applySearch(oTableSearchState);
			}

		},

		onRefresh: function() {
			this._oTable.getBinding("items").refresh();
		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/**
		 * Shows the selected item on the object page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showObject: function(oItem) {
			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("SoId")
			});
		},

		/**
		 * Internal helper method to apply both filter and search state together on the list binding
		 * @param {object} oTableSearchState an array of filters for the search
		 * @private
		 */
		_applySearch: function(oTableSearchState) {
			var oViewModel = this.getModel("worklistView");
			this._oTable.getBinding("items").filter(oTableSearchState, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (oTableSearchState.length !== 0) {
				oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
			}
		},
		//Bayram Hakan
		// ### START VALUE HELP PRODUCT ###

		onProductValueHelpSelected: function() {
			if (!this.valueHelpProduct) {
				this._createValueHelpProduct();
			}
			this._readProducts();
			this.valueHelpProduct.open();

		},
		_createValueHelpProduct: function() {
			if (!this.valueHelpProduct) {
				this.valueHelpProduct = sap.ui
					.xmlfragment(
						"zepm.so.wl.view.fragments.ValueHelpFragments.ProductVH",
						this);
				this.valueHelpProduct.setModel(this.getView()
					.getModel("i18n"), "i18n");
			}
		},
		_readProducts: function(c) {
			var t = this;
			sap.ui.core.BusyIndicator.show(0);
			var mParameters = {
				success: function(d) {

					var jsonModel = new sap.ui.model.json.JSONModel({
						ProductCollection: d.results
					});
					t.valueHelpProduct.setModel(jsonModel);
					sap.ui.core.BusyIndicator.hide();
				},
				error: function(e) {
					jQuery.sap.log
						.error(e.response.body);
					sap.ui.core.BusyIndicator.hide();
				}
			};

			this.oModel.read("/EtProductListSet", mParameters);

		},

		onProductSuggest: function(e) {
			var c = e.getSource();
			var d = function() {
				var m = this.valueHelpProduct.getModel();
				if (m) {
					if (c.getSuggestionItems().length > 0)
						return;
					var C = m.getProperty("/ProductCollection");
					for (var i = 0; i < C.length; i++) {
						var o = C[i];
						var a = new sap.ui.core.CustomData({
							key: "ProductId",
							value: o.ProductId
						});
						var I = new sap.ui.core.Item({
							text: o.Name,
							customData: a
						});
						c.addSuggestionItem(I);
					}
				}

			};

			if (!this.valueHelpProduct) {
				this._createValueHelpProduct();
				this._readProducts(d);
			} else {
				d.call(this);
			}
		},
		onProductSuggestItemSelected: function(e) {
			var I = e.getParameter("selectedItem");
			var r = null;
			for (var i in I.getCustomData()) {
				var c = I.getCustomData()[i];
				if (c.getKey() === "ProductId")
					r = c.getValue();
			}
			this._setProduct(r,I.getText());
			 /* r + " (" + I.getText() + ")" */
		},
		_setProduct: function(r, a) {
			var c = this.byId("idProductIDInput");
			if (c)
				c.setValue(r);

			var b = this.byId("idProductNameVHInput");
			if (b)
				b.setValue(a);
		},
		onProductInputFieldChanged: function(e) {
			var r = e.getSource();
			this._setProduct("", r.getValue());
			var c = function() {
				var m = this.valueHelpProduct.getModel();
				if (m) {
					var R = m.getProperty("/ProductCollection");
					for (var i = 0; i < R.length; i++) {
						var o = R[i];
						if (o.Name.toUpperCase() === r.getValue().toUpperCase()) {
							this._setProduct(o.ProductId,r.getValue() );
							/*o.ProductId + " (" + r.getValue() + ")" */
						}
					}
				}
			};
			if (!this.valueHelpProduct) {
				this._createValueHelpProduct();
				this._readProducts(c);
			} else {
				c.call(this);
			}
		},
		onProductValueHelpSearch: function(e) {
			var s = e.getParameter("value");
			var f = new sap.ui.model.Filter({
				filters: [new sap.ui.model.Filter("ProductId", FilterOperator.Contains, s),
					new sap.ui.model.Filter("Name", FilterOperator.Contains, s)
				],
				bAnd: false
			});
			e.getSource().getBinding("items").filter([f]);
		},
		onProductValueHelpClose: function(e) {
			var s = e.getParameter("selectedItem");
			if (s) {
				var S = s.getBindingContext().getObject();
			
				this._setProduct(S.ProductId , S.Name);
				 /*S.ProductId + " (" + S.Name + ")"*/
			}
			e.getSource().getBinding("items").filter([]);
		},
		onProductValueHelpCancel: function(e) {
			e.getSource().getBinding("items").filter([]);
		},

		// ### END	 VALUE HELP PRODUCT ###
		//Bayram Hakan

	});
});