sap.ui.define([
		"zepm/so/wl/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("zepm.so.wl.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);