jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
		"sap/ui/test/Opa5",
		"zepm/so/wl/test/integration/pages/Common",
		"sap/ui/test/opaQunit",
		"zepm/so/wl/test/integration/pages/Worklist",
		"zepm/so/wl/test/integration/pages/Object",
		"zepm/so/wl/test/integration/pages/NotFound",
		"zepm/so/wl/test/integration/pages/Browser",
		"zepm/so/wl/test/integration/pages/App"
	], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "zepm.so.wl.view."
	});

	sap.ui.require([
		"zepm/so/wl/test/integration/WorklistJourney",
		"zepm/so/wl/test/integration/ObjectJourney",
		"zepm/so/wl/test/integration/NavigationJourney",
		"zepm/so/wl/test/integration/NotFoundJourney",
		"zepm/so/wl/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});