sap.ui.define([], function() {
	"use strict";

	return {
		/**
		 * Rounds the currency value to 2 digits
		 *
		 * @public
		 * @param {string} sValue value to be formatted
		 * @returns {string} formatted currency value with 2 digits
		 */
		currencyValue: function(sValue) {
			if (!sValue) {
				return "";
			}
			return parseFloat((parseFloat(sValue)).toFixed(2));
			//return parseFloat(sValue).toFixed(2);
		},

		numberState: function(sValue) {
			var fValue = parseFloat(sValue).toFixed(2);

			if (fValue >=0 && fValue < 1000.00) {
				return sap.ui.core.ValueState.Error;
			} else if (fValue >= 1000.00 && fValue < 1500.00){
				return sap.ui.core.ValueState.Warning;
			} else {
				return sap.ui.core.ValueState.Success;
			}
		},
		numberUnit: function(sValue) {
			if (!sValue) {
				return "";
			}
			return parseFloat(sValue).toFixed(2);
		},
		dateFormat: function(date) {
			if (date === undefined) return "";
			var oDate = new Date(date);
			var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.yyyy"
			});
			return oDateFormat.format(oDate);
		}
	};

});